var LineChart = function (title, data, context)
{

    this.chartTitle = title;
    this.data = data;

    function drawPath(startEnd, context)
    {
        context.beginPath();
        context.strokeStyle = "green";
        context.moveTo(0, startEnd[0]);
        for (var i = 1; i < startEnd.length; i++)
        {

            context.lineTo(i * 10, startEnd[i]);
        }
        context.stroke();
        context.closePath();


    }
    ;


    function drawAxis(context)
    {
        context.beginPath();
        context.strokeStyle = "black";
        context.moveTo(0.1 * 600, 0.9 * 400);
        context.lineTo(0.9 * 600, 0.9 * 400);

        context.stroke();
        context.closePath();
        console.log(context.width);
        return context;
    }

    function init(title)
    {
        context.font = "20px Arial";
        context.fillText(title, 10, 20);
        drawAxis(context);
    }
    init(this.chartTitle);

};

//form of data
// a=[{key:"thingy", value:"value"}]


//var thingy = new LineChart("My Line Chart", null, context);