var PieChart = function (title, data, context, type) {
    this.chartTitle = title;


// init data
    if (type === "raw")
    {
        var sum = data.reduce(function (previousValue, currentValue, currentIndex, array) {
            return {name: "sum", value: (previousValue.value + currentValue.value)};
        });
//        console.log(sum.value);
        this.data = data.map(function (x) {
            return {name: x.name, value: (x.value / parseInt(sum.value)), color: x.color};
        });
//        console.log(this.data[0].color);
    } else if (type === "pct") {
        this.data = data;
    } else {
        console.log("You entered shit");
        this.data = [0];
    }


    function init(title)
    {
        context.font = "20px Arial";
        context.fillText(title, 10, 20);
    }
    init(this.chartTitle);

    function drawData(data, context)
    {
        context.font = "10px Arial";

        for (var i = 0; i < data.length; i++)
        {
            context.fillText(data[i].name + ": " + data[i].value * 100, 360, 10 * (i + 1));
        }
    }

    drawData(this.data, context);

    function drawChart(data)
    {
        var drawPie = function (rate, color, context)
        {
            context.beginPath();
            context.fillStyle = color;
            context.arc(100, 100, 50, 1.5 * Math.PI, 1.5 * Math.PI + rate * Math.PI * 2);
            context.lineTo(100, 100);
            context.fill();
            context.closePath();
        };



        var cumArray = data.reduce(function (r, a) {
            r.push({name: a.name, value: (((r.length && r[r.length - 1]).value || 0) + a.value), color: a.color});
            return r;
        }, []);

//        console.log(cumArray[2].value);

//        console.log(cumArray.length);
//        for (var i = cumArray.length - 1; i >= 0; i--)
//        {
////            console.log(i);
//            drawPie(cumArray[i].value, cumArray[i].color, context);
//        }
        (cumArray.reverse()).map(function (x) {
            drawPie(x.value, x.color, context);
        });
    }

    drawChart(this.data);


};

var a = [{name: "thingy", value: 100, color: "red"},
    {name: "thingy1", value: 200, color: "blue"},
    {name: "thingy2", value: 400, color: "green"},
    {name: "thingy3", value: 300, color: "black"},
    {name: "thingy4", value: 850, color: "gray"}];


var thingy = new PieChart("LOL Graph", a, context, "raw");